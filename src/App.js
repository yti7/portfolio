import React from "react";

import { 
  About, 
  Header, 
  Footer, 
  Skills, 
  Testimonial, 
  Work
} from "./containers";

import {
  Navbar
} from './components'

const App = () => {
  return (<>
    <Navbar />
    <Header />
    <About />
    <Work />
    <Skills />
    <Testimonial />
    <Footer />
  </>);
};

export default App;
